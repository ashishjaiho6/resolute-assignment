import React from "react";
import { nanoid } from "nanoid";
import {
  collection,
  getDocs,
  doc,
  setDoc,
  deleteDoc,
  getDoc,
} from "firebase/firestore";
import { db } from "../clientApp";
const COLLECTION_NAME = "Students";

export const useStudents = () => {
  const addStudent = async (data) => {
    await setDoc(doc(db, COLLECTION_NAME, nanoid(5)), data).then((res) => {
    });
  };

  const getAllStudents = async (db) => {
    const studentCol = collection(db, COLLECTION_NAME);
    const studentSnap = await getDocs(studentCol);
    let studentsList = [];
    studentSnap.docs.map((doc) => {
      studentsList.push({ ...doc.data(), id: doc.id });
    });
    return studentsList;
  };

  const deleteStudent = async (id) => {
    await deleteDoc(doc(db, COLLECTION_NAME, id)).then((res) => {
    });
  };

  const updateStudent = async (id, data) => {
    await setDoc(doc(db, COLLECTION_NAME, id), data).then((res) => {
    });
  };

  const getStudentById = async (id) => {
    const docRef = doc(db, COLLECTION_NAME, id);
    const docSnap = await getDoc(docRef);
    if (docSnap.exists()) {
      return docSnap.data();
    } else {
    }
  };
  return {
    getStudentById,
    updateStudent,
    deleteStudent,
    addStudent,
    getAllStudents,
  };
};
