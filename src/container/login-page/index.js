import React, { useEffect, useState } from "react";
import styles from "./loginPage.module.css";
import { Button, Card, Form } from "react-bootstrap";
import { ToastContainer, toast } from "react-toastify";
import { signInWithEmailAndPassword } from "firebase/auth";
import { auth } from "../../firebase/clientApp";
import { useRouter } from "next/router";

const LoginPageContainer = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const Route = useRouter();
  useEffect(() => {
    let authToken = localStorage.getItem("AUTH_TOKEN");
    if (authToken) {
      Route.push("/students");
      return;
    }
  });
  const Login = () => {
    signInWithEmailAndPassword(auth, email, password)
      .then((res) => {
        localStorage.setItem("AUTH_TOKEN", res._tokenResponse.refreshToken);
        localStorage.setItem("USER_EMAIL", email);
        Route.push("/students");
        toast.success("Login Successfully !!");
      })
      .catch((err) => {
        toast.error("Some Error !!");
      });
  };

  return (
    <>
      <section className={styles.cardWrapper}>
        <Card className={styles.cardContainer}>
          <Card.Body>
            <h2 className="mb-4 text-center text-decoration-underline">
              Login Here
            </h2>
            <Form>
              <Form.Group className="mb-4">
                <Form.Label>Email address :-</Form.Label>
                <Form.Control
                  type="email"
                  placeholder="Enter email"
                  onChange={(e) => setEmail(e.target.value)}
                  value={email}
                />
              </Form.Group>
              <Form.Group className="mb-4">
                <Form.Label>Password :-</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Password"
                  onChange={(e) => setPassword(e.target.value)}
                  value={password}
                />
              </Form.Group>
              <div className="text-center">
                <Button variant="danger" className="w-50" onClick={Login}>
                  Login
                </Button>
                <ToastContainer />
              </div>
            </Form>
          </Card.Body>
        </Card>
      </section>
    </>
  );
};

export default LoginPageContainer;
