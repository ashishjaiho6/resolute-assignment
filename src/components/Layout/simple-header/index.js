import React, { useState, useEffect } from "react";
import { FiUser } from "react-icons/fi";
import styles from "./header.module.css";
const Header = () => {
  const [email, setEmail] = useState("");
  useEffect(() => {
    let userEmail = localStorage.getItem("USER_EMAIL");
    setEmail(userEmail);
  }, []);

  return (
    <section className={styles.headerWrapper}>
      <div
        className="d-flex justify-content-between align-items-center"
        style={{ height: 40, paddingInline: 70 }}
      >
        <div>
          <div
            style={{
              fontSize: 32,
              fontWeight: "700",
              color: "rgba(88, 74, 74, 0.74)",
            }}
          >
            LOGO
          </div>
        </div>
        <div className={styles.userDetailWrapper} style={{ width: 320 }}>
          <FiUser className="me-3" />
          <div>{email}</div>
        </div>
      </div>
    </section>
  );
};

export default Header;
