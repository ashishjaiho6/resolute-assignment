import React, { useState, useEffect } from "react";
import { Header, SideBar } from "../../components/Layout";
import { SectionHeader } from "../../components/molecules";
import { useStudents } from "../../firebase/api/student";
import useForm from "../../utils/hooks/useForm";
import { Row, Col, Container } from "react-bootstrap";
import { db } from "../../firebase/clientApp";
import { StudentTable, StudentForm } from "../../components/template";
import { useRouter } from "next/router";
import { toast, ToastContainer } from "react-toastify";

const HomePageContainer = () => {
  const { form, setForm, setInForm, handleChange } = useForm();
  const route = useRouter();
  const {
    getStudentById,
    updateStudent,
    deleteStudent,
    addStudent,
    getAllStudents,
  } = useStudents();
  const [activeTab, setActiveTab] = useState("LIST");
  const [refresh, setRefresh] = useState(false);
  const [loading, setLoading] = useState(false);
  const [selectedStudent, setSelectedStudent] = useState("");
  const [students, setStudents] = useState([]);

  useEffect(() => {
    let authToken = localStorage.getItem("AUTH_TOKEN");
    if (authToken) {
    }
    if (!authToken) {
      route.push("/");
    }
    getAllStudents(db).then((res) => {
      setStudents(res);
    });
  }, [refresh]);

  const onViewDetailClick = (studentId) => {
    getStudentById(studentId).then((res) => {
      setForm(res);
      setActiveTab("VIEW_STUDENT_DETAILS");
    });
  };

  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = (id) => {
    setSelectedStudent(id);
    setShow(true);
  };
  const onDeleteClick = (studentId) => {
    setLoading(true);
    deleteStudent(studentId).then((res) => {
      setLoading(false);
      setRefresh(!refresh);
      handleClose();
      setSelectedStudent("");
      toast.success("Deleted Successfully !");
    });
  };

  const onEditClick = (studentId) => {
    setSelectedStudent(studentId);
    getStudentById(studentId).then((res) => {
      setForm(res);
      setActiveTab("EDIT_USER_FORM");
    });
  };

  const onSubmit = () => {
    if (!validate()) {
      return;
    }

    setLoading(true);
    addStudent(form).then((res) => {
      setForm({});
      setLoading(false);
      setRefresh(!refresh);
      // toast.success("Data Added !");
      setActiveTab("LIST");
    });
  };
  const updateStudentDetail = () => {
    if (!validate()) {
      return;
    }
    setLoading(true);
    updateStudent(selectedStudent, form).then((res) => {
      setForm({});
      setLoading(false);
      setRefresh(!refresh);
      setActiveTab("LIST");
      toast.success("Update Successfully !");
    });
  };

  const date = new Date();
  const dateFormatter = Intl.DateTimeFormat("sv-SE");

  const validate = () => {
    if (!form) {
      toast.error("please fill all mendatory fields");
      return;
    }
    if (
      !form.hasOwnProperty("firstName") ||
      !form.hasOwnProperty("classNumber") ||
      !form.hasOwnProperty("rollNumber")
    ) {
      toast.error("please fill all mendatory fields");
      return;
    }
    return true;
  };
  console.log(form, "form");
  return (
    <>
      <Header />
      <Container fluid>
        <Row>
          <Col md={3}>
            <SideBar activeTab={activeTab} setActiveTab={setActiveTab} />
          </Col>
          <Col md={9} style={{ paddingBlock: 30, paddingInline: 20 }}>
            <Container>
              {activeTab === "LIST" && (
                <div>
                  <SectionHeader
                    heading="Manage Students"
                    date={dateFormatter.format(date)}
                  />
                  <StudentTable
                    handleClose={handleClose}
                    handleShow={handleShow}
                    show={show}
                    loading={loading}
                    data={students}
                    onViewDetailClick={onViewDetailClick}
                    onDeleteClick={onDeleteClick}
                    onEditClick={onEditClick}
                    selectedStudent={selectedStudent}
                  />
                  <ToastContainer />
                </div>
              )}
              {activeTab === "USER_FORM" && (
                <div>
                  <SectionHeader
                    heading="Add Student"
                    date={dateFormatter.format(date)}
                  />
                  <StudentForm
                    form={form}
                    handleChange={handleChange}
                    onSubmit={onSubmit}
                    type={activeTab}
                    loading={loading}
                  />
                </div>
              )}
              {activeTab === "EDIT_USER_FORM" && (
                <div>
                  <SectionHeader
                    heading="Edit Student"
                    date={dateFormatter.format(date)}
                  />
                  <StudentForm
                    form={form}
                    handleChange={handleChange}
                    type={activeTab}
                    onSubmit={updateStudentDetail}
                    loading={loading}
                  />
                </div>
              )}
              {activeTab === "VIEW_STUDENT_DETAILS" && (
                <div>
                  <SectionHeader
                    heading="Student Detail"
                    date={dateFormatter.format(date)}
                  />
                  <StudentForm
                    form={form}
                    handleChange={handleChange}
                    type={activeTab}
                    onSubmit={() => {
                      setForm({});
                      setActiveTab("LIST");
                    }}
                    disabled
                  />
                </div>
              )}
            </Container>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default HomePageContainer;
