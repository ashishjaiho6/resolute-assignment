import React from "react";
import { FiList, FiLogOut, FiUsers } from "react-icons/fi";
import styles from "./sidebar.module.css";
import { useRouter } from "next/router";
const SideBar = ({ activeTab, setActiveTab }) => {
  const route = useRouter();
  const logout = () => {
    localStorage.removeItem("AUTH_TOKEN");
    route.push("/");
  };
  return (
    <section className={styles.sidebarWrapper}>
      <div>
        <div
          className={`${activeTab == "USER_FORM" ? styles.menuActive : ""} ${
            styles.menu
          } `}
          onClick={() => {
            setActiveTab("USER_FORM");
          }}
        >
          <FiUsers className={styles.menuIcon} />
          <div>Add Student</div>
        </div>
        <div
          className={`${activeTab == "LIST" ? styles.menuActive : ""} ${
            styles.menu
          } `}
          onClick={() => {
            setActiveTab("LIST");
          }}
        >
          <FiList className={styles.menuIcon} />
          <div>Manage Student</div>
        </div>
        <div
          className={styles.menu}
          onClick={() => {
            logout();
          }}
        >
          <FiLogOut className={styles.menuIcon} />
          <div>Logout</div>
        </div>
      </div>
    </section>
  );
};

export default SideBar;
