import { StudentForm } from "./student-form";
import { StudentTable } from "./student-table";

export { StudentForm, StudentTable };
