import React, { useState } from "react";
import styles from "./student-table.module.css";
import { FiEdit3, FiEye } from "react-icons/fi";
import { RiDeleteBin6Line, RiDeleteBinLine } from "react-icons/ri";
import { Table, Button, Modal, Spinner, Image } from "react-bootstrap";
export const StudentTable = ({
  data = [],
  onViewDetailClick,
  onDeleteClick,
  onEditClick,
  handleClose,
  handleShow,
  loading,
  show,
  selectedStudent,
}) => {
  return (
    <section>
      <Table
        striped
        hover
        style={{ borderRadius: "5px" }}
        className={styles.tableBody}
      >
        <thead className={styles.tableHeader}>
          <tr>
            <th>Name</th>
            <th>Class</th>
            <th>Roll No.</th>
            <th className={styles.editHeader}>View / Edit / Delete</th>
          </tr>
        </thead>
        <tbody>
          {data.map((student, index) => {
            const {
              firstName = "",
              lastName = "",
              middleName = "",
              division = "",
            } = student;
            return (
              <tr key={index}>
                <td>
                  {firstName} {middleName} {lastName}
                </td>
                <td>{student.classNumber}</td>
                <td>{student.rollNumber}</td>
                <td className={styles.redIcon}>
                  <FiEye
                    className={styles.actionIcon}
                    onClick={() => onViewDetailClick(student.id)}
                  />
                  <FiEdit3
                    className={`${styles.actionIcon} ${styles.iconMargin}`}
                    onClick={() => onEditClick(student.id)}
                  />
                  <RiDeleteBin6Line
                    className={styles.actionIcon}
                    onClick={() => handleShow(student.id)}
                  />
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>

      <Modal show={show} centered>
        <Modal.Body className="text-center">
          <Modal.Title>Are You Sure !</Modal.Title>
          <div className="my-5">
            <Image height={100} width={100} src="/images/rightIMG.png" />
          </div>
          <div className="d-flex w-50 m-auto justify-content-between align=item-center">
            <Button variant="success" onClick={handleClose}>
              Cancel
            </Button>
            <Button
              variant="danger"
              onClick={() => {
                onDeleteClick(selectedStudent);
              }}
            >
              {loading ? (
                <Spinner animation="border" variant="light" size="sm" />
              ) : (
                "Delete"
              )}
            </Button>
          </div>
        </Modal.Body>
      </Modal>
    </section>
  );
};
