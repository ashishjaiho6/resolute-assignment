import React from "react";
import { Col, Row, Form, Button, Spinner } from "react-bootstrap";
import styles from "./studentForm.module.css";

export const StudentForm = ({
  form,
  handleChange,
  onSubmit,
  type,
  disabled = false,
  loading,
}) => {
  const getButtonText = () => {
    let text = "";
    if (type == "VIEW_STUDENT_DETAILS") {
      text = "Go Back";
    } else if (type == "USER_FORM") {
      text = "Add Students";
    } else if (type == "EDIT_USER_FORM") {
      text = "Update Students";
    }

    return text;
  };

  const range = (start, stop, step) =>
    Array.from(
      { length: (stop - start) / step + 1 },
      (_, i) => start + i * step
    );
  return (
    <section>
      <Row className="mb-3">
        <Col md={4}>
          <Form.Control
            disabled={disabled}
            placeholder="Enter Name"
            name="firstName"
            onChange={handleChange}
            value={form?.firstName}
          />
        </Col>
        <Col md={4}>
          <Form.Control
            disabled={disabled}
            placeholder="Middle Name"
            name="middleName"
            onChange={handleChange}
            value={form?.middleName}
          />
        </Col>
        <Col md={4}>
          <Form.Control
            disabled={disabled}
            placeholder="Last Name"
            name="lastName"
            onChange={handleChange}
            value={form?.lastName}
          />
        </Col>
      </Row>
      <Row className="mb-5">
        <Col md={4}>
          <Form.Select
            aria-label="Select Class"
            disabled={disabled}
            onChange={handleChange}
            value={form?.classNumber}
            name="classNumber"
            placeholder="Select Class"
          >
            {range(0, 12, 1).map((item, i) => {
              return (
                <option value={item} key={item}>
                  {item}
                </option>
              );
            })}
          </Form.Select>
        </Col>
        <Col md={4}>
          <Form.Select
            aria-label="Select Division"
            disabled={disabled}
            onChange={handleChange}
            value={form?.devision}
            name="devision"
            placeholder="Select Division"
          >
            {["A", "B", "C", "E", "F"].map((item, i) => {
              return (
                <option value={item} key={item}>
                  {item}
                </option>
              );
            })}
          </Form.Select>
        </Col>
        <Col md={4}>
          <Form.Control
            disabled={disabled}
            placeholder="Enter Roll Number in Digits"
            name="rollNumber"
            onChange={handleChange}
            value={form?.rollNumber}
          />
        </Col>
      </Row>
      <Row className="mb-3">
        <Col md={6}>
          <Form.Control
            disabled={disabled}
            as="textarea"
            placeholder="Address Line 1"
            name="addressOne"
            onChange={handleChange}
            value={form?.addressOne}
          />
        </Col>
        <Col md={6}>
          <Form.Control
            disabled={disabled}
            as="textarea"
            placeholder="Address Line 2"
            name="addressTwo"
            onChange={handleChange}
            value={form?.addressTwo}
          />
        </Col>
      </Row>
      <Row className="mb-4">
        <Col md={4}>
          <Form.Control
            disabled={disabled}
            placeholder="Landmark"
            name="landMark"
            onChange={handleChange}
            value={form?.landMark}
          />
        </Col>
        <Col md={4}>
          <Form.Control
            disabled={disabled}
            placeholder="City"
            name="city"
            onChange={handleChange}
            value={form?.city}
          />
        </Col>
        <Col md={4}>
          <Form.Control
            disabled={disabled}
            maxLength={6}
            type="number"
            placeholder="Pin Code"
            name="pinCode"
            onChange={handleChange}
            value={form?.pinCode}
          />
        </Col>
      </Row>
      <Row>
        <Col md={4}>
          <Button
            disabled={loading}
            className={`${styles.addButton} btn-danger`}
            onClick={() => onSubmit(form?.id)}
          >
            {loading ? (
              <Spinner animation="border" variant="light" size="sm" />
            ) : (
              getButtonText()
            )}
          </Button>
        </Col>
      </Row>
    </section>
  );
};
