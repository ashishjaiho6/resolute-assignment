// import { firebase } from "@firebase/app";
// import "@firebase/auth";
// import "@firebase/firestore";

// <------- DEV MODE  --------->

let config = {
  apiKey: "AIzaSyCJboM98A7XZzAiKi-12QdlGaNevIqniDM",
  authDomain: "resolute-assignment-f81bf.firebaseapp.com",
  projectId: "resolute-assignment-f81bf",
  storageBucket: "resolute-assignment-f81bf.appspot.com",
  messagingSenderId: "89730228842",
  appId: "1:89730228842:web:6f87bfc39a8fe15fd1a013",
  measurementId: "G-C7M7L9W9D3",
};

// <------- PRD MODE  --------->

// let config = {
//   apiKey: process.env.NEXT_PUBLIC_API_KEY,
//   authDomain: process.env.NEXT_PUBLIC_AUTH_DOMAIN,
//   projectId: process.env.NEXT_PUBLIC_PROJECT_ID,
//   storageBucket: process.env.NEXT_PUBLIC_STORAGE_BUCKET,
//   messagingSenderId: process.env.NEXT_PUBLIC_MESSAGING_SENDER_ID,
//   appId: process.env.NEXT_PUBLIC_APP_ID,
// };

// let app;
// if (!firebase.apps.length) {
//   app = firebase.initializeApp(config);
// }

// const app = firebase.initializeApp(config);
// app?.firestore()?.settings({ experimentalForceLongPolling: true });

// export const db = app;

import { initializeApp, getApps } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getAuth } from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyCJboM98A7XZzAiKi-12QdlGaNevIqniDM",
  authDomain: "resolute-assignment-f81bf.firebaseapp.com",
  projectId: "resolute-assignment-f81bf",
  storageBucket: "resolute-assignment-f81bf.appspot.com",
  messagingSenderId: "89730228842",
  appId: "1:89730228842:web:6f87bfc39a8fe15fd1a013",
  measurementId: "G-C7M7L9W9D3",
};

if (!getApps().length) {
}

const app = initializeApp(firebaseConfig);

const db = getFirestore(app);
const auth = getAuth(app);

export { db, auth };
