import React from "react";
import styles from "./section-header.module.css";
const SectionHeader = ({ heading, date }) => {
  return (
    <section>
      <div
        style={{ height: 40 }}
        className="d-flex justify-content-between align-items-between"
      >
        <div className={styles.tableTitle}>{heading}</div>
        <div className={styles.tableDate}>{date}</div>
      </div>
    </section>
  );
};

export default SectionHeader;
